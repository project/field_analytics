<?php

/**
 * @file
 * Administrative interface for field analytics.
 */

/**
 * Menu callback: Field analytics per-bundle settings form.
 */
function field_analytics_bundle_settings_form($form, &$form_state, $entity_type, $bundle) {
  $bundle = field_extract_bundle($entity_type, $bundle);

  // Gather field information.
  $instances = field_info_instances($entity_type, $bundle);

  // Build an options array.
  $options = array();
  foreach ($instances as $instance) {
    $allowed_values = field_analytics_get_allowed_values($instance);

    // Only include fields with allowed values as exposed filters.
    if (!empty($allowed_values)) {
      $options[$instance['field_name']] = $instance['label'];
    }
  }

  $form["field_analytics_filter_fields_{$entity_type}_{$bundle}"] = array(
    '#type' => 'checkboxes',
    '#title' => t('Filterable fields'),
    '#options' => $options,
    '#default_value' => variable_get("field_analytics_filter_fields_{$entity_type}_{$bundle}", array()),
    '#description' => t('Select which fields will be exposed filters for field analytics reports on this bundle.'),
  );

  return system_settings_form($form);
}

/**
 * Menu callback: Shows an analysis of a field for every applicable bundle.
 *
 * @param array $instance
 *   The menu-loaded field instance.
 *
 * @see field_ui_menu_load()
 * @see field_ui_field_edit_form()
 */
function field_analytics_field_analytics($instance) {
  $report = new FieldAnalyticsReports($instance);

  $form = drupal_get_form('field_analytics_filter_form', $report);

  $output = '';

  $output .= drupal_render($form);

  $output .= $report->reports();

  return $output;
}

/**
 * Field analytics Entity Field Query filter form.
 *
 * @todo in Drupal 8 use object methods as Form API callbacks directly.
 */
function field_analytics_filter_form($form, &$form_state, FieldAnalyticsReports $object) {
  $form_state['object'] = $object;
  return $object->filterForm($form, $form_state);
}

/**
 * Submit callback for field_analytics_filter_form().
 *
 * This is the callback automagically added by drupal_prepare_form().
 */
function field_analytics_filter_form_submit($form, &$form_state) {
  $object = $form_state['object'];
  return $object->filterFormSubmit($form, $form_state);
}

/**
 * Submit callback for field_analytics_filter_form().
 *
 * Resets the Field analytics filter form.
 */
function field_analytics_filter_form_reset($form, &$form_state) {
  $object = $form_state['object'];
  return $object->filterFormReset($form, $form_state);
}
