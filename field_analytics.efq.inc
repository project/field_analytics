<?php

/**
 * Helper EFQ class extension for field analytics reports.
 */
class FieldAnalyticsQuery extends EntityFieldQuery {

  /**
   * Define some defaults for a reuseable field analytics query.
   *
   * @param string $instance
   *   A field instance.
   */
  public function __construct($instance) {
    $this
      ->entityCondition('entity_type', $instance['entity_type'])
      ->entityCondition('bundle', $instance['bundle']);

    // Allow this query to be filtered by query parameters. This assumes that
    // the 'filters' query is an associative array of field values, keyed by
    // names of fields in the same bundle.
    $filters = isset($_GET['filters']) ? $_GET['filters'] : array();
    if (!empty($filters)) {
      foreach($filters as $field_name => $value) {
        // Get field info so we know how to filter it.
        $field = field_info_field($field_name);
        $column = field_analytics_get_value_column($field);

        $this->fieldCondition($field_name, $column, $value, '=');
      }
    }
  }

  /**
   * Helper function to find not null fields.
   *
   * This function exists because EntityFieldQuery::fieldCondition() does not
   * support `IS NULL`. As for naming, follow DatabaseCondition::isNotNull().
   *
   * See this @link http://drupal.stackexchange.com/a/10488 summary. @endlink
   *
   * Using '<>' seems to work. Also see related documentation in
   * EntityFieldQuery::addFieldCondition().
   *
   * @param string|array $field
   *   The entity field name or field array.
   * @param string $column
   *   (optional) Column that should hold a value. Defaults to 'value'.
   */
  public function isNotNull($field, $column = 'value') {
    return $this->fieldCondition($field, $column, '', '<>');
  }

}
