<?php

/**
 * Value entered.
 */
class FieldAnalyticsHighchartsReportValueEntered extends FieldAnalyticsReportValueEntered {

  function data() {
    $series = array();

    // Value entered.
    $value = new stdClass();
    $value->name = t('Value entered');
    $value->data = array((int)$this->countHasValue());
    $series[] = $value;

    // No value entered.
    $no_value = new stdClass();
    $no_value->name = t('No value entered');
    $no_value->data = array($this->countEntities() - $this->countHasValue());
    $series[] = $no_value;

    return $series;
  }

  function render() {
    $data = $this->data();

    #$template = new HighchartsOptionsBarBasic($data);
    $template = new HighchartsOptionsBarStacked($data);
    $options = $template->render();

    // Unset chart title and category title.
    $options->title->text = '';
    $options->xAxis->categories = array('');

    $output = '';
    $output .= parent::render();
    $output .= '<h2>' . t('Values entered totals') . '</h2>';
    $output .= highcharts_render($options);

    return $output;
  }

}

/**
 * Allowed values.
 */
class FieldAnalyticsHighchartsReportAllowedValues extends FieldAnalyticsReportAllowedValues {

  function data() {
    return parent::data();
  }

  function render() {
    // This could go in data() but if we want to include parent::render() here,
    // we need to keep data() return value consistent with parent::data().
    $parent_data = $this->data();
    $data = array();
    // Format the data as percent for the pie chart.
    foreach ($parent_data as $info) {
      $percent = $this->getPercent($info['count'], $this->countHasValue());
      $data[] = array($info['label'], $percent);
    }

    // Render allowed values as a pie basic chart.
    //$data = $this->data();
    $chart = new HighchartsOptionsPieBasic($data);
    $options = $chart->render();

    // Make adjustments to the options object.
    // Remove the title, since we have a header already.
    $options->title->text = '';

    // Build the output.
    $output = '';

    // Render base report analytics.
    $output .= parent::render();

    // Render the chart representation.
    $output .= '<h2>' . t('Allowed values percentage') . '</h2>';
    $output .= highcharts_render($options);

    return $output;
  }

}

/**
 * Average value.
 */
class FieldAnalyticsHighchartsReportAverageValue extends FieldAnalyticsReportAverageValue {

  function data() {
    parent::data();
  }

  function render() {
    return parent::render();
  }

}

/**
 * Average length.
 */
class FieldAnalyticsHighchartsReportAverageLength extends FieldAnalyticsReportAverageLength {

  function data() {
    parent::data();
  }

  function render() {
    return parent::render();
  }

}
